# OVERLAY

Construcción manual de una red entre contenedores

## Explicación

Para llevar a cabo el proyecto ha sido necesaria la creación de un script con nombre contman.

Se presupone que el host tendrá instalado y configurado docker.

El script acepta los siguientes parámetros:

* contman -h (Muestra los comandos)	

* contman arrange -s <subnet> -i <HOST_INTERFACE>

* contman deploy -c <container_IP> -n <container_name> -s <subnet> -h <HOST_IP> -v <VETH_PAIR>
	
* contman remove	-n <container_name>
	

### Arrange
Con la opción arrange se configura el espacio de trabajo previamente a desplegar los nodos, es necesario incluir la subnet en formato 192.168.X.X/24 y la interfaz del host (eth0 | enp0s3 | ... ).
En este apartado se lleva a cabo la creación de la imagen docker, se crea y configura el puente y se crea y configura el overlay vxlan.

Para permitir la conectividad con la red también se han añadido reglas de forwarding para el puente.

### Deploy
Para la opción deploy es necesario incluir muchos más parámetros un ejemplo de ejecución sería:
bash contman deploy -c 192.168.111.11 -n ubuntu1 -s 192.168.111.0/24 -h 192.168.1.151 -v veth1

Esta opción se encarga de poner en marcha los contenedores con la imagen creada previamente, posteriormente obtiene acceso al namespace del contenedor creado para poder ejecutar los comandos dentro del network namespace.

Para el Escenario 1 se crean las parejas veth, se le asocia una ip a una de ellas y la otra se asocia al puente creado en el paso previo. Para permitir la salida de paquetes se establece una regla de enrutamiento dentro de cada network namespace.

Para el Escenario 2 se hace uso de enmascaramiento de la ip, para así permitir la sustitución de la ip privada por la ip del host. Para permitir el acceso desde fuera es posible añadir en el contrario una regla de encaminamiento o como en mi caso agregar reglas de port forwarding para permitir el acceso a un puerto en concreto.

El Escenario 3 ha sido llevado a cabo a nivel L3 usando VXLAN que simula una red L2 sobre una L3. Se han hecho uso de los veth previos y el bridge para así no duplicar infraestructura.

### Remove
Esta opción permite eliminar un contenedor pasando su nombre por parámetro
